# Algorithms

> My codes for important Algorithms and Data Structures

## Overview

Algorithms are the basic programming fundamentals that is required to solve complex problems. Use of Data Structures make it easier in implementation of Complex problems. Correct usage of Data Structures in the correct place is essentially required. This project will include all the problems done by me as a part of competitive coding or simple applications of Algorithm and Data Structures. 

## Motivation

1. Knack of Programming 
2. Build up skills and programming fundamentals.
3. Increase Problem Solving ability.

## Languages Used

1. C++
2. C
3. Java

## Getting Started

All codes are compiled and executed. Feel free to run in your machine and test it.

## Contributors

Anu Kumari Gupta - CSE

## License

MIT © 2018 Anu Kumari Gupta 





